﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniGames
{
    public class Menu
    {
        public Menu()
        {
            Console.WriteLine("MiniGames Menu\r");
            Console.WriteLine("------------------");
            Console.WriteLine("Choose a program to run from the following list:");
            Console.WriteLine("\t1 - HangMan");
            Console.WriteLine("\t2 - TicTacToe");
            Console.WriteLine("\t3 - PlaceHolder");
            Console.WriteLine("\t4 - PlaceHolder");
            Console.WriteLine("\tQ - Exit");
            Console.Write("choice? ");
        }

    }
}
