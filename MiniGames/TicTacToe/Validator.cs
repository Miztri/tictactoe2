﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniGames.TicTacToe
{
    public class Validator
    {
        public static bool Marker(string marker)
        {
            marker = marker.ToUpper();
            return marker == GlobalConstant.XMarker || marker == GlobalConstant.OMarker;
        }

        public static bool TurnOrder(string turnOrder)
        {
            return turnOrder == "1" || turnOrder == "2";
        }

        public static bool Move(string move, string[] spaces)
        {
            int index;
            bool isANumber = Int32.TryParse(move, out index);
            return isANumber ? MoveIsInboundsAndSpaceIsEmpty(index, spaces) : false;
        }
       
        private static bool MoveIsInboundsAndSpaceIsEmpty(int index, string[] spaces)
        {
            return InBoundsMove(index, spaces.Length) && BoardState.IsAnEmptySpace(spaces[index]);
        }

        private static bool InBoundsMove(int index, int spacesLength)
        {
            return index < spacesLength && index >= 0;
        }
    }
}
