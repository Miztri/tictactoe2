﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniGames.TicTacToe
{
    public static class GlobalConstant
    {        
        public const string XMarker = "X";
        public const string OMarker = "O";
    }
}
