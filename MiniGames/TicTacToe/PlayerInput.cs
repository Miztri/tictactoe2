﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniGames.TicTacToe
{
    public class PlayerInput
    {
        public static string GetPlayerName()
        {
            GameHandler.GetPlayerName();
            return GameHandler.ReadInput();
        }

        public static int GetPlayerMove(string name, string[] spaces)
        {
            GameHandler.GetPlayersMove(name);
            string move = GameHandler.ReadInput();

            if (!Validator.Move(move, spaces))
            {
                GameHandler.InvalidMove(move);
                return GetPlayerMove(name, spaces);
            }
            else
            {
                return ConvertMoveToIndex(move);
            }

        }

        public static string GetTurnOrder(string name)
        {
            GameHandler.GetTurnOrder(name);
            string turnOrder = GameHandler.ReadInput();
            if (!Validator.TurnOrder(turnOrder))
            {
                GameHandler.InvalidMove(turnOrder);
                return GetTurnOrder(name);
            }
            else
            {
                return turnOrder;
            }
        }


        public static string GetInput(Action message, Func<string, bool> validator)
        {
            message();
            string input = GameHandler.ReadInput();
            if (!validator(input))
            {
                GameHandler.InvalidMove(input);
                return GetInput(message, validator);
            }

            else
            {
                return input.ToUpper();
            }
        }

        private static int ConvertMoveToIndex(string move)
        {
            return Int32.Parse(move);
        }
    }
}
