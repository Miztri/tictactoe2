﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniGames.TicTacToe
{
    public class GameHandler
    {
        public static void GetPlayerName()
        {
            Console.WriteLine("What is your name?");
        }

        public static void GetPlayersMove(string name)
        {
            Console.WriteLine("Where would you like to move " + name + "?");
        }

        public static void GetPlayerMarker()
        {
            Console.WriteLine("What piece would you like to be, X or O?");
        }

        public static void GetTurnOrder(string name)
        {
            Console.WriteLine("Type 1 if you would like " + name + " to go first, and 2 to go second");
        }

        public static void CreateGameBoard(string[] spaces)
        {
            string board = "";
            string[][] rows = BoardState.Rows(spaces);
            int lengthOfRow = BoardState.WidthOfBoard(spaces);

            for (int currentRowIndex = 0; currentRowIndex < rows.Length; currentRowIndex += 1)
            {
                string row = "";
                for (int currentSpaceIndex = 0; currentSpaceIndex < rows.Length; currentSpaceIndex += 1)
                {
                    int index = currentRowIndex * lengthOfRow + currentSpaceIndex;
                    row = string.Format(@" {0} {1} | ", row, spaces[index]);
                }

                board = string.Format(@" {0} 
                                         {1} ", board, row);
            }

            Console.WriteLine(board);
        }

        public static void GameWinner(string name)
        {
            Console.WriteLine(name + " has won the game");
        }

        public static void GameTied()
        {
            Console.WriteLine("The game is a tie");
        }

        public static void InvalidMove(string input)
        {
            Console.WriteLine(input + " is not a valid input");
        }        
        
        public static string ReadInput()
        {
            return Console.ReadLine();
        }
    }
}

