﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniGames.TicTacToe
{
    public class GameRules
    {
        public static bool Won(string[] spaces)
        {
            return BoardState.AnySetsTheSame(spaces);
        }

        public static bool Over(string[] spaces)
        {
            return Won(spaces) || BoardState.AllSpacesNotEmpty(spaces);
        }
    }
}
