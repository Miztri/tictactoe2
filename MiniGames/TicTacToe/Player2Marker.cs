﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniGames.TicTacToe
{
    public class Player2Marker
    {
        public static string Marker(string marker)
        {
            return marker == GlobalConstant.XMarker ? GlobalConstant.OMarker : GlobalConstant.XMarker;
        }
    }
}
