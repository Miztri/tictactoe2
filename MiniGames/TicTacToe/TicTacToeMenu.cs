﻿using MiniGames.TicTacToe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniGames
{
    class TicTacToeMenu
    {
        public TicTacToeMenu()
        {
            Start();           
        }
        private GameBoard board;
        private GameSetUp GameSetUp;
        private Player[] players;

        public void Start()
        {
            ReadGameModeAndInitializePlayers();
            Setup();
            Player lastPlayerToMove = Moves();
            WonOrTiedMessage(lastPlayerToMove);
        }

        private void Setup()
        {
            
            GameSetUp = new GameSetUp(players);
            board = new GameBoard(GameSetUp.GetBoardDimmensions());
            GameSetUp.Start();
            players = GameSetUp.players;
            
        }

        private void ReadGameModeAndInitializePlayers()
        {
            InitializeHumanPlayers();                      
        }

        private Player Moves()
        {
            Player currentPlayer = FirstPlayer();
            Turn(currentPlayer);
            while (!GameRules.Over(board.GetSpaces()))
            {
                currentPlayer = currentPlayer == FirstPlayer() ? SecondPlayer() : FirstPlayer();
                Turn(currentPlayer);
            }
            GameHandler.CreateGameBoard(board.GetSpaces());
            return currentPlayer;

        }

        private void WonOrTiedMessage(Player lastPlayerToMove)
        {
            if (GameRules.Won(board.GetSpaces()))
            {
                GameHandler.GameWinner(lastPlayerToMove.GetName());
                Console.Read();
            }
            else
            {
                GameHandler.GameTied();
                Console.Read();
            }
        }

        private void Turn(Player currentPlayer)
        {
            GameHandler.CreateGameBoard(board.GetSpaces());
            int move = currentPlayer.Move(board.GetSpaces());
            Console.Clear();
            MarkBoard(move, currentPlayer.GetMarker());

        }


        private void InitializeHumanPlayers()
        {
            players = new Player[] { new Turn(), new Turn() };
        }               

        private Player FirstPlayer()
        {
            return players.First();
        }

        private Player SecondPlayer()
        {
            return players.Last();
        }

        private void MarkBoard(int space, string marker)
        {
            board.Mark(space, marker);
        }
    }
}
