﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniGames.TicTacToe
{
    public class Turn : Player
    {
        public override int Move(string[] spaces)
        {
            return PlayerInput.GetPlayerMove(GetName(), spaces);
        }
    }
}
