﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniGames.TicTacToe
{
    class GameSetUp
    {
        public Player[] players { get; private set; }
        private int boardDimmensions;

        public GameSetUp(Player[] players)
        {
            this.players = players;
        }

        public int GetBoardDimmensions()
        {
            return boardDimmensions = 3;
        }

        public void SetBoardDimmensions(string boardDimmensions)
        {
            this.boardDimmensions = Int32.Parse(boardDimmensions);
        }

        public void Start()
        {
            SetPlayerName(players.First(), PlayerInput.GetPlayerName());
            string marker = PlayerInput.GetInput(GameHandler.GetPlayerMarker, Validator.Marker);
            SetPlayerMarker(players.First(), marker);
            SetSecondPlayerName();
            SetPlayerMarker(players.Last(), Player2Marker.Marker(PlayerMarker(players.First())));
            AssignTurnOrder(PlayerInput.GetTurnOrder(PlayerName(players.First())));
        }

        private void SetSecondPlayerName()
        {
            bool twoPlayerGame = players.Last() is Turn;

            if (twoPlayerGame)
            {
                SetPlayerName(players.Last(), PlayerInput.GetPlayerName());
            }
            else
            {
                players.Last().AssignName("Johnny 5");
            }
        }

        private void SetPlayerName(Player player, string name)
        {
            player.AssignName(name);
        }

        private void SetPlayerMarker(Player player, string marker)
        {
            player.AssignMarker(marker);
        }

        private void AssignTurnOrder(string turnOrder)
        {
            if (turnOrder == "2")
            {
                players = new Player[] { players.Last(), players.First() };
            }
        }

        private string PlayerMarker(Player player)
        {
            return player.GetMarker();
        }

        private string PlayerName(Player player)
        {
            return player.GetName();
        }
    }
}
