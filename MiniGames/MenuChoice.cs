﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniGames
{
    public class MenuChoice
    {     

        public static void Main(string[] args)
        {
            MenuChoice menuChoice = new MenuChoice();
            bool endApp = false;           

            while (!endApp)
            {                
                Menu ShowMenu = new Menu();
                string choice = menuChoice.Input();

                switch (choice)
                {
                    case "1":
                        Console.Clear();
                        HangManMenu hangMan = new HangManMenu();
                        break;
                    case "2":
                        Console.Clear();
                        TicTacToeMenu ticTacToeMenu = new TicTacToeMenu();
                        break;
                    case "3":
                        
                        break;
                    case "4":
                        break;
                    case "Q":
                        endApp = QuittingApp();
                        break;
                    default:
                        break;
                }
                Console.Clear();                
            }
        }

        public string Input()
        {
            string choice = Console.ReadLine().ToUpper();
            return choice;
        }

        private static bool QuittingApp()
        {
            Console.Write("Are you sure you want to quit? y/n ");
            if (Console.ReadLine() == "y")
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }
}

