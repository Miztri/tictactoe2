﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniGames
{
    public class HangManCore
    {
        MenuChoice menuChoice = new MenuChoice();

        public HangManCore()
        {
            string[] words = { "Testing", "That", "words", "Work" };
            int TurnLimit = 10;
            Console.WriteLine("Playing");
            string word = ChooseWord(words);
            List<Pair<char, bool>> charcheck = new List<Pair<char, bool>>();
            foreach (char c in word)
            {
                charcheck.Add(new Pair<char, bool>(c, false));
            }

            while (TurnLimit > 0 && !CheckDict(charcheck))
            {
                WriteWord(charcheck);
                string UserChoice = menuChoice.Input();
                if (UserChoice.Length > 1)
                {
                    if (CheckWord(charcheck, UserChoice))
                        break;
                }
                else
                {
                    PutUserChoice(ref charcheck, UserChoice);
                    TurnLimit--;
                }
            }
            if (TurnLimit == 0)
            {
                Console.WriteLine("No Turns Left");
                Console.ReadLine();
            }
            else
            {
                WriteWord(charcheck);
                Console.WriteLine("You Guessed Right");
                Console.ReadLine();
            }

        }

        public void CheckWord()
        {
            throw new NotImplementedException();
        }

        public bool CheckWord(List<Pair<char, bool>> dict, string userChoice)
        {
            if(dict.Count == userChoice.ToCharArray().Length)
            {
                for (int i = 0; i < dict.Count; i++)
                {
                    Pair<char, bool> p = dict[i];
                    if (p.Item1.ToString().ToLower() == userChoice[i].ToString().ToLower())
                        continue;
                    else
                        return false;
                }
                return true;
            }
            return false;
        }

        public string ChooseWord(string[] words)
        {
            return words[(new Random().Next(0, words.Length - 1))];
        }

        public bool CheckDict(List<Pair<char, bool>> dict)
        {
            foreach(Pair<char,bool> d in dict)
            {
                if (!d.Item2)
                {
                    return false;
                } 
            }
            return true;
        }

        public void WriteWord(List<Pair<char,bool>> dict)
        {
            foreach (Pair<char,bool> vP in dict)
            {
                if (vP.Item2)
                {
                    Console.Write(vP.Item1);
                }
                else
                {
                    Console.Write("_");
                }
            }

            Console.WriteLine();
        }

        public void PutUserChoice( ref List<Pair<char,bool>> dict, string userChoice)
        {
            foreach(Pair<char,bool> tpl in dict)
            {
                if (tpl.Item1.ToString().ToLower() == userChoice.ToLower())
                {
                    tpl.Item2 = true;
                }
            }
        }

    }  
}
