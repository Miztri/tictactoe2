﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniGames
{
    class HangManMenu
    {
        public HangManMenu()
        {
            MenuChoice menuChoice = new MenuChoice();
            bool submenu = false;
            while (!submenu)
            {
                Console.WriteLine("Welcome To HangMan");
                Console.WriteLine("\t1 - Play");
                Console.WriteLine("\tQ - Main Menu");

                Console.Write("choice? ");

                string choice = menuChoice.Input();
                switch (choice)
                {
                    case "1":
                        Console.Clear();
                        HangManCore Play = new HangManCore();                        
                        break;                   
                    case "Q":
                        Console.Clear();
                        submenu = true;
                        break;
                    default:
                        break;
                }
                Console.Clear();
            }
        }
    }
}
