﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniGames
{
    public class Pair<T1, T2>
    {
        public T1 Item1 { get; set; }
        public T2 Item2 { get; set; }

        public Pair (T1 v1, T2 v2)
        {
            this.Item1 = v1;
            this.Item2 = v2;
        }

    }
}
