﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using MiniGames.TicTacToe;
using System.IO;

namespace MiniGamesTest
{
    [TestFixture]
    public class ValidatorTests
    {
        [Test]
        public void ReturnsTrueIfMarkerIsValid()
        {
            Assert.IsTrue(Validator.Marker("X"));
        }

        [Test]
        public void ReturnsFalseIfMarkerIsNotValid()
        {
            Assert.IsFalse(Validator.Marker("P"));
        }

        [Test]
        public void ReturnsTrueIfSpaceIsAvailable()
        {
            string[] spaces = { "0", "X", "2", "3", "O", "5", "6", "7", "8" };
            Assert.IsTrue(Validator.Move("0", spaces));
        }

        [Test]
        public void ReturnsFalseIfSpaceIsNotAvailable()
        {
            string[] spaces = { "0", "X", "2", "3", "O", "5", "6", "7", "8" };
            Assert.IsFalse(Validator.Move("1", spaces));
        }

        [Test]
        public void ReturnFalseIfNotAnActualSpace()
        {
            string[] spaces = { "0", "X", "2", "3", "O", "5", "6", "7", "8" };
            Assert.IsFalse(Validator.Move("9", spaces));
        }  
    }
    [TestFixture]
    public class PlayerTests
    {
        [Test]
        public void CanAssignAndRetrieveAPlayersName()
        {
            Turn turn = new Turn();

            turn.AssignName("Bob");

            Assert.AreEqual("Bob", turn.GetName());
        }

        [Test]
        public void CanAssignAndRetrieveAPlayerPiece()
        {
            Turn turn = new Turn();

            turn.AssignMarker("X");

            Assert.AreEqual("X", turn.GetMarker());
        }

        [Test]
        public void CanMakeAMove()
        {
            Turn turn = new Turn();
            string[] spaces = { "0", "1", "2", "3", "4", "5", "6", "7", "8" };

            TicTacToeTestHelper.SetInput("4");

            Assert.AreEqual(4, turn.Move(spaces));
        }
    }
    [TestFixture]
    public class GameBoardTests
    {

        GameBoard gameboard;

        [SetUp]
        public void CreateBoardWithDimmensions()
        {
            gameboard = new GameBoard(3);
        }

        [Test]
        public void SpacesAreEmptyUponCreation()
        {
            string[] emptyBoard = { "0", "1", "2", "3", "4", "5", "6", "7", "8" };

            Assert.AreEqual(emptyBoard, gameboard.GetSpaces());
        }

        [Test]
        public void MarksSpaceWithGivenPiece()
        {
            gameboard.Mark(4, "X");

            Assert.AreEqual("X", gameboard.GetSpaceAt(4));
        }

    }
    [TestFixture]
    public class Player2MarkerTests
    {
        [Test]
        public void ReturnsTheOppositeMarker()
        {
            Assert.AreEqual("O", Player2Marker.Marker("X"));
        }
    }
    [TestFixture]
    public class PlayerInputTest
    {

        [Test]
        public void ReturnsAName()
        {
            TicTacToeTestHelper.SetInput("Kirby\n");

            string name = PlayerInput.GetPlayerName();

            Assert.AreEqual("Kirby", name);
        }

        [Test]
        public void ReturnsAMarker()
        {
            TicTacToeTestHelper.SetInput("X\n");

            string marker = PlayerInput.GetInput(GameHandler.GetPlayerMarker, Validator.Marker);

            Assert.AreEqual("X", marker);
        }

        [Test]
        public void AsksForMarkerAgainIfInvalid()
        {
            TicTacToeTestHelper.SetInput("P\nO\n");

            string marker = PlayerInput.GetInput(GameHandler.GetPlayerMarker, Validator.Marker);

            Assert.AreNotEqual("P", marker);
            Assert.AreEqual("O", marker);
        }


        [Test]
        public void ReturnsAMove()
        {
            TicTacToeTestHelper.SetInput("4\n");
            string[] spaces = { "0", "1", "2", "3", "4", "5", "6", "7", "8" };
            int move = PlayerInput.GetPlayerMove("Bob", spaces);

            Assert.AreEqual(4, move);
        }

        [Test]
        public void DoesNotAcceptInvalidMove()
        {
            TicTacToeTestHelper.SetInput("fake move!\n2\n");
            string[] spaces = { "0", "1", "2", "3", "4", "5", "6", "7", "8" };
            int move = PlayerInput.GetPlayerMove("Robert", spaces);

            Assert.AreEqual(2, move);
            Assert.AreNotEqual("fake move", move);
        }       
    }
    [TestFixture]
    public class GameHandlerTests
    {

        public StringWriter CaptureTheOutput()
        {
            StringWriter sw = new StringWriter();
            Console.SetOut(sw);
            return sw;
        }

        [Test]
        public void GameHandlerAskForMove()
        {
            StringWriter sw = CaptureTheOutput();
            GameHandler.GetPlayersMove("Robert");

            StringAssert.Contains("Where would you like to move Robert?", sw.ToString());
        }

        [Test]
        public void GameHandlerAskForAName()
        {
            StringWriter sw = CaptureTheOutput();
            GameHandler.GetPlayerName();

            StringAssert.Contains("What is your name?", sw.ToString());
        }

        [Test]
        public void GameHandlerAskForAMarker()
        {
            StringWriter sw = CaptureTheOutput();
            GameHandler.GetPlayerMarker();

            StringAssert.Contains("What piece would you like to be, X or O?", sw.ToString());
        }

        [Test]
        public void GameHandlerAskForTurnOrder()
        {
            StringWriter sw = CaptureTheOutput();
            GameHandler.GetTurnOrder("Tony");
            string expected = "Type 1 if you would like Tony to go first, and 2 to go second";

            StringAssert.Contains(expected, sw.ToString());
        }

        [Test]
        public void GameHandlerAPrintedVersionOfTheBoard()
        {
            StringWriter sw = CaptureTheOutput();
            string[] spaces = { "0", "1", "2", "3", "4", "5", "6", "7", "8" };
            GameHandler.CreateGameBoard(spaces);
            string expected =
                @"0 |  1 |  2 |";

            StringAssert.Contains(expected, sw.ToString());
        }

        [Test]
        public void APartiallyFilledAndPrintedBoard()
        {
            StringWriter sw = CaptureTheOutput();
            string[] spaces = { "X", "O", "2", "3", "4", "5", "6", "7", "8" };
            GameHandler.CreateGameBoard(spaces);
            string expected =
                @"X |  O |  2";

            StringAssert.Contains(expected, sw.ToString());

        }

        [Test]
        public void AMessageForTheWinner()
        {
            StringWriter sw = CaptureTheOutput();
            string winnersMessage = "Robert has won the game";
            GameHandler.GameWinner("Robert");

            StringAssert.Contains(winnersMessage, sw.ToString());
        }

        [Test]
        public void AMessageForATieGame()
        {
            StringWriter sw = CaptureTheOutput();
            string tiedMessage = "The game is a tie";
            GameHandler.GameTied();

            StringAssert.Contains(tiedMessage, sw.ToString());
        }

        [Test]
        public void AMessageForInvalidInput()
        {
            StringWriter sw = CaptureTheOutput();
            string invalidMessage = "P is not a valid input";
            GameHandler.InvalidMove("P");

            StringAssert.Contains(invalidMessage, sw.ToString());
        }        
    }
    [TestFixture]
    public class BoardStateTests
    {
        [Test]
        public void SpaceThatReturnMarkersAreNotEmpty()
        {
            string space = "X";
            bool result = BoardState.IsAnEmptySpace(space);

            Assert.IsFalse(result);
        }

        [Test]
        public void SpaceThatReturnsANonMarkerIsNotEmpty()
        {
            string space = "4";
            bool result = BoardState.IsAnEmptySpace(space);

            Assert.IsTrue(result);
        }

        [Test]
        public void ChecksIfAllSpacesAreNotEmpty()
        {
            string[] spaces = { "X", "O", "X", "X", "X", "X", "X", "X", "X" };
            bool result = BoardState.AllSpacesNotEmpty(spaces);

            Assert.IsTrue(result);
        }

        [Test]
        public void CheckIfAllSpacesAreNotEmpty()
        {
            string[] spaces = { "X", "X", "X", "X", "X", "X", "X", "X", "8" };
            bool result = BoardState.AllSpacesNotEmpty(spaces);

            Assert.IsFalse(result);
        }

        [Test]
        public void CanFindRowsColumnsAndDiagonals()
        {
            string[] spaces = {"0", "1", "2",
                               "3", "4", "5",
                               "6", "7", "8"};
            string[][] rowsColumnsDiagonals = new string[8][];
            rowsColumnsDiagonals[0] = new string[] { "0", "1", "2" };
            rowsColumnsDiagonals[1] = new string[] { "3", "4", "5" };
            rowsColumnsDiagonals[2] = new string[] { "6", "7", "8" };
            rowsColumnsDiagonals[3] = new string[] { "0", "3", "6" };
            rowsColumnsDiagonals[4] = new string[] { "1", "4", "7" };
            rowsColumnsDiagonals[5] = new string[] { "2", "5", "8" };
            rowsColumnsDiagonals[6] = new string[] { "0", "4", "8" };
            rowsColumnsDiagonals[7] = new string[] { "2", "4", "6" };

            CollectionAssert.AreEqual(rowsColumnsDiagonals, BoardState.RowsColumnsDiagonals(spaces));
        }

        [Test]
        public void ReturnTrueIfAnySetsAreTheSame()
        {
            string[] spaces = { "X", "X", "X", "3", "4", "5", "6", "7", "8" };

            Assert.IsTrue(BoardState.AnySetsTheSame(spaces));
        }

        [Test]
        public void ReturnFalseIfAnySetsAreTheSame()
        {
            string[] spaces = { "X", "X", "2", "3", "4", "5", "6", "7", "8" };

            Assert.IsFalse(BoardState.AnySetsTheSame(spaces));
        }

        [Test]
        public void ReturnAvailableSpaces()
        {
            string[] spaces = { "X", "X", "2", "3", "4", "5", "6", "7", "8" };
            string[] expected = { "2", "3", "4", "5", "6", "7", "8" };

            Assert.AreEqual(expected, BoardState.AvailableSpaces(spaces));
        }
    }
    [TestFixture]
    public class GameRulesTests
    {
        [Test]
        public void GameIsOverWhenBoardIsFilledAndNotWon()
        {
            string[] spaces = { "X", "O", "X",
                                "X", "X", "O",
                                "O", "X", "O" };
            Assert.IsTrue(GameRules.Over(spaces));
        }

        [Test]
        public void GameIsNotOverWhenBoardIsEmpty()
        {
            string[] spaces = { "0", "1", "2",
                                "3", "4", "5",
                                "6", "7", "8" };
            Assert.IsFalse(GameRules.Over(spaces));
        }

        [Test]
        public void GameIsOverWhenGameIsWon()
        {
            string[] spaces = { "X", "X", "X",
                                "3", "4", "5",
                                "6", "7", "8"};
            Assert.IsTrue(GameRules.Over(spaces));
        }

        [Test]
        public void GameIsWonWhenOneMarkerHasFilledTopRow()
        {
            string[] spaces = { "X", "X", "X",
                                "3", "4", "5",
                                "6", "7", "8" };
            Assert.IsTrue(GameRules.Won(spaces));
        }

        [Test]
        public void GameIsNotWonTopRowIsFilledWithMixedMarkers()
        {
            string[] spaces = { "X", "O", "X",
                                "3", "4", "5",
                                "6", "7", "8" };
            Assert.IsFalse(GameRules.Won(spaces));
        }

        [Test]
        public void GameIsWonWhenSecondRowIsFilledWithOneMarker()
        {
            string[] spaces = { "0", "1", "2",
                                "X", "X", "X",
                                "6", "7", "8" };
            Assert.IsTrue(GameRules.Won(spaces));
        }

        [Test]
        public void GameIsWonWhenThirdRowIsFilledWithOneMarker()
        {
            string[] spaces = { "0", "1", "2",
                                "3", "4", "5",
                                "X", "X", "X" };
            Assert.IsTrue(GameRules.Won(spaces));
        }

        [Test]
        public void GameIsWonWhenFirstColumnIsFilledWithOneMarker()
        {
            string[] spaces = { "O", "1", "2",
                                "O", "4", "5",
                                "O", "7", "8"};
            Assert.IsTrue(GameRules.Won(spaces));
        }

        [Test]
        public void GameIsWonWhenSecondColumnIsFilledWithOneMarker()
        {
            string[] spaces = {"0", "X", "2",
                               "3", "X", "5",
                               "6", "X", "8"};
            Assert.IsTrue(GameRules.Won(spaces));
        }

        [Test]
        public void GameIsWonWhenThirdColumnIsFilledWithOneMarker()
        {
            string[] spaces = {"0", "1", "X",
                               "3", "4", "X",
                               "6", "7", "X"};
            Assert.IsTrue(GameRules.Won(spaces));
        }

        [Test]
        public void GameIsWonWhenFirstDigonalIsFilledWithOneMarker()
        {
            string[] spaces = {"X", "1", "2",
                               "3", "X", "5",
                               "6", "7", "X"};
            Assert.IsTrue(GameRules.Won(spaces));
        }

        [Test]
        public void GameIsWonWhenSecondDigonalIsFilledWithOneMarker()
        {
            string[] spaces = {"0", "1", "X",
                               "3", "X", "5",
                               "X", "7", "8"};
            Assert.IsTrue(GameRules.Won(spaces));
        }
    }

}
