﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit;
using NUnit.Framework;
using MiniGames;

namespace MiniGamesTest
{
    [TestFixture]
    public class HangManTests
    {
        HangManCoreService game = new HangManCoreService();
        [Test]
        public void IsTheCorrectLetterBeingInput()
        {           
           
            game.CheckWord("training");

            bool isFound = game.UserInput("z");

            Assert.AreEqual(true, isFound, "something");
        }

        
    }
}
